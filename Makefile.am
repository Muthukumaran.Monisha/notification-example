ACLOCAL_AMFLAGS = -I m4 ${ACLOCAL_FLAGS}

AM_CPPFLAGS = \
	-I$(top_srcdir) \
	-I$(top_builddir) \
	-DG_LOG_DOMAIN=\"NotificationExample\" \
	-DPKGDATADIR=\"$(pkgdatadir)\" \
	$(NULL)

AM_CFLAGS = \
	$(WARN_CFLAGS) \
	$(CODE_COVERAGE_CFLAGS) \
	$(GLIB_CFLAGS) \
	$(CLUTTER_CFLAGS) \
	$(THORNBURY_CFLAGS) \
	$(MILDENHALL_CFLAGS) \
	$(NULL)

AM_LDFLAGS = \
	-no-undefined \
	$(WARN_LDFLAGS) \
	$(NULL)

SUBDIRS = .
bin_PROGRAMS =
lib_LTLIBRARIES =
noinst_LTLIBRARIES =
CLEANFILES =

# AppStream
$(metainfo_DATA): %: %.in Makefile
	$(AM_V_GEN) sed \
		-e "s|[@]BUNDLE_ID[@]|$(BUNDLE_ID)|" \
		-e "s|[@]NTE_VERSION[@]|$(NTE_VERSION)|" \
		$< > $@.tmp && mv $@.tmp $@

metainfodir = $(datadir)/metainfo
metainfo_DATA = \
	notification-example/$(BUNDLE_ID).metainfo.xml \
	$(NULL)

CLEANFILES += $(metainfo_DATA)

RESOURCE_DIR = notification-example/resources
RESOURCE_FILES = $(RESOURCE_DIR)/icons/notification_icon.png
RESOURCE_XML = $(RESOURCE_DIR)/NotificationExample.gresource.xml
# compile gresources
notification-example/notification-example-gresources.c: $(RESOURCE_XML) $(RESOURCE_FILES)
	$(GLIB_COMPILE_RESOURCES) $(RESOURCE_XML) --target=$@ --sourcedir=$(RESOURCE_DIR) --generate-source

# desktop file
$(applications_DATA): %: %.in Makefile
	$(AM_V_GEN) sed \
		-e "s|[@]BUNDLE_ID[@]|$(BUNDLE_ID)|" \
		-e "s|[@]prefix[@]|$(prefix)|" \
		-e "s|[@]bindir[@]|$(bindir)|" \
		$< > $@.tmp && mv $@.tmp $@

applicationsdir = $(datadir)/applications
applications_DATA = \
	notification-example/$(BUNDLE_ID).desktop \
	$(NULL)

CLEANFILES += $(applications_DATA)

# appaprmor file
$(apparmor_policy_DATA): %: %.in Makefile
	$(AM_V_GEN) sed \
		-e "s|[@]BUNDLE_ID[@]|$(BUNDLE_ID)|" \
		-e "s|[@]prefix[@]|$(prefix)|" \
		$< > $@.tmp && mv $@.tmp $@

apparmor_policydir = $(sysconfdir)/apparmor.d
apparmor_policy_DATA = \
	notification-example/Applications.$(BUNDLE_ID) \
	$(NULL)

CLEANFILES += $(apparmor_policy_DATA)

dist_pkgdata_DATA = \
	notification-example/resources/button_speller_text_prop.json \
	$(NULL)

resources_icondir = $(datadir)/icons/hicolor/32x32/apps/
dist_resources_icon_DATA = \
	notification-example/resources/icons/$(BUNDLE_ID).png \
	$(NULL)

bin_PROGRAMS += notification-example/notification-example

notification_example_notification_example_SOURCES = \
	notification-example/main.c \
	notification-example/notification-example.c \
	notification-example/notification-example-gresources.c \
	$(NULL)

notification_example_notification_example_LDADD = \
	$(CODE_COVERAGE_LIBS) \
	$(THORNBURY_LIBS) \
	$(MILDENHALL_LIBS) \
	$(NULL)

EXTRA_DIST = \
	$(metainfo_DATA:%=%.in) \
	$(apparmor_policy_DATA:%=%.in) \
	$(applications_DATA:%=%.in) \
	.arclint \
	.clang-format \
	notification-example.doap \
	autogen.sh \
	tap-test \
	glib-tap.mk \
	$(NULL)

DISTCHECK_CONFIGURE_FLAGS = --enable-introspection --enable-documentation

@GENERATE_CHANGELOG_RULES@
dist-hook: dist-ChangeLog

# Code coverage
@CODE_COVERAGE_RULES@
CODE_COVERAGE_DIRECTORY =
CODE_COVERAGE_LCOV_OPTIONS = --base-directory $(abs_top_srcdir)

GITIGNOREFILES = \
	m4/ \
	build-aux/ \
	$(NULL)

MAINTAINERCLEANFILES = \
	$(GITIGNORE_MAINTAINERCLEANFILES_TOPLEVEL) \
	$(GITIGNORE_MAINTAINERCLEANFILES_MAKEFILE_IN) \
	$(GITIGNORE_MAINTAINERCLEANFILES_M4_LIBTOOL) \
	INSTALL \
	$(NULL)

-include $(top_srcdir)/git.mk
