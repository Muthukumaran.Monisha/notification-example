/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"
#include "notification-example.h"

#include <gio/gio.h>
#include <mildenhall/mildenhall.h>

#define RESOURCE_PREFIX "/org/apertis/Examples/Notification"

struct _HlwApp
{
  /*< private >*/
  GApplication parent;

  ClutterActor *stage;                  /* owned */
  MildenhallButtonSpeller *button;      /* unowned */
};

G_DEFINE_TYPE (HlwApp, hlw_app, G_TYPE_APPLICATION);

enum
{
  COLUMN_TEXT = 0,
  COLUMN_LAST = COLUMN_TEXT,
  N_COLUMN
};

static gboolean
on_stage_destroy (ClutterActor *stage,
                  gpointer user_data)
{
  GApplication *app = user_data;

  g_debug ("The main stage is destroyed.");

  /* chain up */
  CLUTTER_ACTOR_GET_CLASS (stage)->destroy (stage);

  /* The main window is detroyed (closed) so the application
   * needs to be released */
  g_application_release (app);

  return TRUE;
}

static GBytes *
load_icon_bytes (void)
{
  GError *err = NULL;
  GBytes *data = g_resources_lookup_data (
    RESOURCE_PREFIX "/icons/notification_icon.png",
    G_RESOURCE_LOOKUP_FLAGS_NONE, &err);

  if (data == NULL)
  {
    g_error ("Data lookup failed: %s", err->message);
    return NULL;
  }
  return data;
}

static gboolean
notification_timeout_cb (gpointer app)
{
  g_application_withdraw_notification (G_APPLICATION (app), "example");

  return G_SOURCE_REMOVE;
}

static void
notify (GApplication *app)
{
  g_autoptr (GNotification) notification = NULL;
  g_autoptr (GIcon) icon = NULL;
  g_autoptr (GBytes) bytes = NULL;

  notification = g_notification_new ("Time to notify");
  g_notification_set_body (notification, "This is an example notification");

  bytes = load_icon_bytes ();
  icon = g_bytes_icon_new (bytes);

  g_notification_set_icon (notification, icon);
  g_application_send_notification (app, "example", notification);
   
  g_timeout_add_seconds (2, notification_timeout_cb, app);
}

static void
button_pressed_cb (MildenhallButtonSpeller *button,
                   gpointer user_data)
{
  HlwApp *self = HLW_APP (user_data);
  notify (G_APPLICATION (self));
}

static void
startup (GApplication *app)
{
  g_autoptr (GObject) widget_object = NULL;
  g_autoptr (ThornburyItemFactory) item_factory = NULL;
  ThornburyModel *model;
  g_autofree gchar *widget_properties_file = NULL;

  HlwApp *self = HLW_APP (app);

  g_application_hold (app);

  /* chain up */
  G_APPLICATION_CLASS (hlw_app_parent_class)->startup (app);

  /* build ui components */
  self->stage = mildenhall_stage_new (g_application_get_application_id (app));

  g_signal_connect (G_OBJECT (self->stage),
      "destroy", G_CALLBACK (on_stage_destroy),
      self);

  widget_properties_file = g_build_filename (PKGDATADIR,
                                             "button_speller_text_prop.json",
                                             NULL);

  item_factory = thornbury_item_factory_generate_widget_with_props (
      MILDENHALL_TYPE_BUTTON_SPELLER,
      widget_properties_file);

  g_object_get (item_factory,
                "object", &widget_object,
                NULL);

  self->button = MILDENHALL_BUTTON_SPELLER (widget_object);

  model = THORNBURY_MODEL (
      thornbury_list_model_new (N_COLUMN,
                                G_TYPE_STRING,
                                NULL,
                                -1));

  thornbury_model_append (model, 
                          COLUMN_TEXT, "Click to Notify",
                          -1);

  g_object_set (self->button,
                "model", model,
                NULL);

  g_object_unref (model);

  clutter_actor_add_child (self->stage, CLUTTER_ACTOR (self->button));

  /* connect button listeners */
  g_signal_connect (self->button, "button-press",
    G_CALLBACK (button_pressed_cb), self);
}

static void
activate (GApplication *app)
{
  HlwApp *self = HLW_APP (app);
  clutter_actor_show (self->stage);
}

static void
hlw_app_dispose (GObject *object)
{
  HlwApp *self = HLW_APP (object);

  g_clear_pointer (&self->stage, clutter_actor_destroy);

  G_OBJECT_CLASS (hlw_app_parent_class)->dispose (object);
}

static void
hlw_app_class_init (HlwAppClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  object_class->dispose = hlw_app_dispose;

  app_class->startup = startup;
  app_class->activate = activate;
}

static void
hlw_app_init (HlwApp *self)
{
}
